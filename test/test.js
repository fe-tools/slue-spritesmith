const slue = require('slue');
const slueSpritesmith = require('../index');
slue.read('./all.css')
    .pipe(slueSpritesmith({
        // 源图位置
        imagePath: './sprites/',
        // 合成后图片放置位置
        spritePath: './output/images/sprite.png',
        // 处理后的css放置位置
        cssPath: './output/css/all.css'
    }))
    //.pipe(slue.write('./output/'));