const fs = require('fs');
const os = require('os');
const fse = require('fs-extra');
const path = require('path');
const spritesmith = require('spritesmith');
const slueStream = require('slue-stream');

module.exports = function(opts = {}) {
    return slueStream.transformObj(function(file, env, cb) {
        let imagePath = path.resolve(process.cwd(), opts.imagePath);
        let dirname = path.dirname(file.path);
        let searchRegular = /(background|background-image)\s*:[^;\{\}]*url\(([\'\"]?)[\w\-\/\.]+\2\)[^;\{\}]*;/g;
        let urlRegular = /(background|background-image)\s*:[^;\{\}]*url\(([\'\"]?)([\w\-\/\.]+)\2\)[^;\{\}]*;/;
        let contents = file.contents.toString();
        let matchResult = contents.match(searchRegular);

        if (matchResult) {
            let sprites = [];
            matchResult.forEach(function(item) {
                let urlMatchResult = item.match(urlRegular);
                if (urlMatchResult) {
                    let imgPath = path.resolve(dirname, urlMatchResult[3]);
                    if (imgPath.indexOf(imagePath) == 0) {
                        sprites.push(imgPath);
                    }
                }
            });

            sprites = sprites.filter(function(file) {
                return fs.existsSync(file);
            });

            if (sprites.length) {
                let data = {
                    src: sprites,
                    padding: opts.padding || 4
                };

                spritesmith.run(data, function(err, res) {
                    let cssPath = path.resolve(process.cwd(), opts.cssPath);
                    let cssDirname = path.dirname(cssPath);
                    let spritePath = path.resolve(process.cwd(), opts.spritePath);
                    let spriteDirname = path.dirname(spritePath);
                    let spriteBasename = path.basename(spritePath);
                    let relativePath = path.relative(cssDirname, spriteDirname);
                    relativePath = relativePath.replace(/\\/g, '/');
                    relativePath = relativePath || '.';
                    relativePath = `${relativePath}/${spriteBasename}`;
                    if (res) {
                        fse.mkdirpSync(cssDirname);
                        fse.mkdirpSync(spriteDirname);
                        contents = contents.replace(searchRegular, function(matchResult) {
                            let urlMatchResult = matchResult.match(urlRegular);
                            if (urlMatchResult) {
                                let imgPath = path.resolve(dirname, urlMatchResult[3]);
                                if (res.coordinates[imgPath]) {
                                    return `${urlMatchResult[1]}:url(${relativePath});${os.EOL}background-position:-${res.coordinates[imgPath].x}px -${res.coordinates[imgPath].y}px;${os.EOL}`;
                                }
                            }
                            return matchResult;
                        });

                        fs.writeFileSync(cssPath, contents);
                        fs.writeFileSync(spritePath, res.image);

                        cb(null, file);
                    } else {
                        console.log(err);
                        cb(null, file);
                    }
                });
            } else {
                cb(null, file);
            }
        } else {
            cb(null, file);
        }
    });
}