# slue-spritesmith

Slue plugin to concat icons by scan some css files, and then to modify the css file.

## Usage

```javascript
const slue = require('slue');
const slueSpriteSmith = require('slue-spritesmith');

slue.task('spritesmith', function() {
    return slue.read('./src/**/*.css')
        .pipe(slueSpritesmith({
            // the path of source icons
            imagePath: './src/assets/images/sprite',
            // the path of the concated image
            spritePath: './dev/assets/images/sprite.png',
            // where the midified files to save
            cssPath: './dev'
        }))
});
```